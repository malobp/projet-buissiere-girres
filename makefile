EXE = programme_abouti.exe
CC  = gcc
SRC = $(wildcard *.c)
OBJ = bin/calcul_trajectoire.o bin/demander_parametres.o bin/affichage.o bin/main.o
LIB = lib/lire_nombre_positif.a
all : $(EXE)
#$@ pour la cible , $< pour la premiere dependance, $^ pour ttes les dependances

lib : 
	mkdir lib
	
lib/%.a : bin/%.o
	ar rcs $@ $<

bin : 
	mkdir bin
	
bin/%.o : src/aboutissement/%.c
	$(CC) -o $@ -c $< -Wall
	
$(EXE) : $(OBJ) $(LIB)
	$(CC) -o $@ $^ -lm

clean : 
	rm $(OBJ) $(LIB)