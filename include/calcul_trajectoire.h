typedef struct coordonnees_s {
        float x;
        float y;
        float z;
}coord;

coord nouveau_point (float x, float y, float z);

coord addition_coordonnees(coord p1, coord p2);

coord vitesse_lorenz (coord point, float * parametre1, float * parametre2, float * parametre3);

coord vitesse_trajectoire_ballistique (float * parametre1, float * parametre2, float * parametre3, float * parametre4, float t);

coord vitesse (coord point, float* parametre1, float* parametre2, float* parametre3, float* parametre4, int numero_du_systeme, float t);

coord position_suivante (coord point, float * dt, float * parametre1, float * parametre2, float * parametre3, float* parametre4, int numero_du_systeme, float t);

void fabrication_tableau(coord position_vals[], coord vitesse_vals[], float * dt, float * parametre1, float * parametre2, float * parametre3, float * parametre4, int numero_du_systeme, float x, float y, float z, float nb_de_points);