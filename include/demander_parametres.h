
void choix_systeme_dynamique (int *numero_du_systeme);
void demander_parametres_lorenz (float*teta , float*kho , float*beta);
void demander_parametres_generaux (float*x0 , float*y0 , float*z0 , float*Tmax , float*dt, int * nb_de_fleches);
void demander_parametres_trajectoire_ballistique ( float*g , float*vx , float*vy , float*vz);