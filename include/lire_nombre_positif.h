void lire_float_positif (float*);
void lire_float (float*);
void lire_entier_positif (int *x);
int lire_fin_ligne ();
// Nous déclarons ici toutes les fonctions qui seront utilisées pour lire les paramètres rentrés par l'utilisateur.