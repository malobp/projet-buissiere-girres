#include <stdio.h>
#include <malloc.h>
#include "../../include/calcul_trajectoire.h"

coord nouveau_point (float x, float y, float z)
{
    coord res;
    res.x=x;
    res.y=y;
    res.z=z;
    return res;
}

coord addition_coordonnees(coord p1, coord p2)
{
    return nouveau_point((p1.x + p2.x), (p1.y + p2.y), (p1.z + p2.z));
}

//On prend en entrée des fonctions les adresses des paramètres et non les paramètres eux-mêmes pour rester cohérents dans les notations, même si en réalité on n'utilise que les valeurs des paramètres.
coord vitesse_lorenz (coord point, float * parametre1, float * parametre2, float * parametre3)
{
    return nouveau_point ((* parametre1) * (point.y - point.x), point.x * ((*parametre2) - point.z) - point.y, point.x * point.y - (* parametre3) * point.z);
}

coord vitesse_trajectoire_ballistique (float *parametre1, float *parametre2, float *parametre3, float *parametre4, float t)
{
    return nouveau_point(*parametre1, *parametre2, (*parametre3 - *parametre4 * t));
}

coord vitesse (coord point, float* parametre1, float* parametre2, float* parametre3, float* parametre4, int numero_du_systeme, float t)
{
    coord vitesse_en_un_point;
    if (numero_du_systeme == 1)
    {
        vitesse_en_un_point = vitesse_lorenz ( point , parametre1, parametre2, parametre3 );
    }
    if (numero_du_systeme == 2)
    {
        vitesse_en_un_point = vitesse_trajectoire_ballistique ( parametre1, parametre2, parametre3, parametre4, t );
    }
    return vitesse_en_un_point;
}

coord position_suivante (coord point , float *dt, float *parametre1, float *parametre2, float *parametre3, float *parametre4, int numero_du_systeme, float t)
{
    coord vit = vitesse (point, parametre1 , parametre2 , parametre3 , parametre4 , numero_du_systeme , t );
    return addition_coordonnees( point , nouveau_point((*dt) * vit.x , (*dt) * vit.y , (*dt) * vit.z)) ;
}

void fabrication_tableau (coord position_vals[], coord vitesse_vals[], float* dt, float* parametre1, float* parametre2, float* parametre3, float* parametre4 , int numero_du_systeme, float x0, float y0, float z0, float nb_de_points)
{
    int i;
    float t= (*dt);
    
    position_vals[0] = nouveau_point (x0,y0,z0);
    vitesse_vals[0] = vitesse (position_vals[0], parametre1 , parametre2 , parametre3 , parametre4 , numero_du_systeme , 0 );
    
    for (i=1; i<(int)nb_de_points; i++)
    {
        position_vals[i]=position_suivante(position_vals[i-1], dt , parametre1 , parametre2 , parametre3 , parametre4 , numero_du_systeme , t );
        vitesse_vals[i] = vitesse (position_vals[i-1], parametre1 , parametre2 , parametre3 , parametre4 , numero_du_systeme , t );
        t=t+(*dt);
    }
}