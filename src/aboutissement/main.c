#include "../../include/calcul_trajectoire.h"
#include "../../include/demander_parametres.h"
#include "../../include/affichage.h"
#include <stdio.h>
#include <stdlib.h>

int main ()
{
    float x0 = 0;
    float y0 = 0;
    float z0 = 0;
    float dt = 0;
    float Tmax = 0; 
    int nb_de_fleches = 0;
    int numero_du_systeme = 0;
    float parametre1 = 0;
    float parametre2 = 0;
    float parametre3 = 0;
    float parametre4 = 0;
    coord *position_vals;
    coord *vitesse_vals;
    demander_parametres_generaux( &x0, &y0 , &z0 , &Tmax , &dt, &nb_de_fleches);
    nb_de_fleches++;
    while ( numero_du_systeme != 1 && numero_du_systeme != 2 )
        {
            choix_systeme_dynamique(&numero_du_systeme);
        }
    printf(" x0 = %f, y0 = %f, z0 = %f, dt = %f , Tmax = %f\n" , x0 , y0 , z0 , dt , Tmax) ;
    if ( numero_du_systeme == 1 )
        {

            demander_parametres_lorenz ( &parametre1 , &parametre2 , &parametre3 ) ;  
            printf("Vous avez choisi le système régi par les équations de Lorenz :\n") ;
            printf(" theta = %f , kho = %f , beta = %f\n" , parametre1 , parametre2 , parametre3)   ;
        }
    else
        {
            demander_parametres_trajectoire_ballistique (&parametre1 , &parametre2 , &parametre3 , &parametre4 )   ;
            printf("Vous avez choisi le système de trajectoire ballistique :\n") ;
            printf(" g = %f, vx = %f, vy = %f, vz = %f\n" , parametre4 , parametre1 , parametre2 , parametre3 ) ;
        }
     //Au vu de l'utilisation qu'on fait du nombre de points total, ici il est intéressant d'en faire un flottant.
    float nb_de_points = (Tmax)/(dt);
    position_vals = (coord*)malloc((int)nb_de_points * sizeof(coord));
    vitesse_vals = (coord*)malloc((int)nb_de_points * sizeof(coord));
    fabrication_tableau(position_vals, vitesse_vals, &dt, &parametre1, &parametre2, &parametre3, &parametre4, numero_du_systeme, x0, y0, z0, nb_de_points);
    gnuplotable_fabricator (position_vals, vitesse_vals, &Tmax , &dt , &nb_de_fleches, nb_de_points);
    affichage();
    return 0;
}