#include "../../include/lire_nombre_positif.h"
#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>

int lire_fin_ligne(){
    int nonblancs = 0, c = getchar();
    while((c != EOF) && (c != '\n')){
        if(!isspace(c)){
            nonblancs++;
        }
        c = getchar();
    }
    return nonblancs;
}

void lire_float_positif (float *x)
{
    int nonblancs, lu;
    do
    {
        lu = scanf("%f",x);
        nonblancs=lire_fin_ligne();
    } while ((lu!=1) || (nonblancs>0) || (*x<=0));
}

void lire_entier_positif (int *x)
{
    int nonblancs, lu;
    do
    {
        lu = scanf("%d",x);
        nonblancs=lire_fin_ligne();
    } while ((lu!=1) || (nonblancs>0) || (*x<0));
}

void lire_float (float *x)
{
    int nonblancs, lu;
    do
    {
        lu = scanf("%f", x);
        nonblancs=lire_fin_ligne();
    } while ((lu!=1) || (nonblancs>0));
}

/*int main(){
    float x;
    lire_float(&x);
    printf("%f", x);
    return 0;
}*/