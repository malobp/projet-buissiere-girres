#include "../../include/calcul_trajectoire.h"
#include "../../include/affichage.h"
#include <stdio.h>
#include <stdlib.h>
#include <math.h>

void gnuplotable_fabricator (coord position_vals[], coord vitesse_vals[], float *Tmax, float *dt, int *nb_de_fleches , float nb_de_points )
{
    FILE * f;
    FILE * g;
    g = fopen ("configur.gnu", "w");
    f = fopen ("fichier_tableau_pour_gnuplot.dat", "w");
    fprintf (g, "set parametric\n");
    fprintf (g, "set terminal png\n");
    fprintf (g, "set output 'courbe.png'\n");
    int i;
    float compteur_t=0;
    float compteur_fleches=0;
    float distance_parcourue=0;
    for (i=0; i<(int)nb_de_points; i++)
    {
        distance_parcourue = distance_parcourue + (*dt) *(float)sqrt( (double)((vitesse_vals[i].x) *(vitesse_vals[i].x)  +(vitesse_vals[i].y) *(vitesse_vals[i].y) + (vitesse_vals[i].z) *(vitesse_vals[i].z)));
    }
    
//Le compteur_fleches ne compte pas du tout les flèches, mais les points desquels on doit afficher la vitesse pour répartir les flèches le plus harmonieusement possible.
     for (i=0; i<(int)nb_de_points; i++)
    {
        fprintf (f, "%f %f %f %f\n", compteur_t, position_vals[i].x, position_vals[i].y, position_vals[i].z);
    }
    if (nb_de_fleches!=0 && *nb_de_fleches!=1)
    {
        float taille_fleche= distance_parcourue/(*nb_de_fleches);
        for (i=0; i<(int)nb_de_points; i++)
        {
            if (compteur_t >= compteur_fleches)
            {
                fprintf (g, "set arrow from %f,%f,%f to %f,%f,%f\n", position_vals[i].x, position_vals[i].y, position_vals[i].z, position_vals[i].x + taille_fleche * (*dt) *vitesse_vals[i].x, position_vals[i].y + taille_fleche *(*dt)* vitesse_vals[i].y, position_vals[i].z + taille_fleche * (*dt) *vitesse_vals[i].z);
                compteur_fleches = compteur_fleches + (*dt)*nb_de_points/((*nb_de_fleches)-1);
            }
            compteur_t= compteur_t + (*dt);
        }
    }
    fprintf (g, "show arrow\n");
    fprintf (g, "splot 'fichier_tableau_pour_gnuplot.dat' using 2:3:4 with lines\n");
    fclose (f);
    fclose (g);

}
void affichage()
{
    /*fopen (f, "r");
    fopen (g, "r");*/
    system ("gnuplot > load 'configur.gnu'");
    /*fclose (f);
    fclose(g);*/
}


