#include "../../include/lire_nombre_positif.h"
#include "../../include/demander_parametres.h"
#include <stdio.h>
void choix_systeme_dynamique (int *numero_du_systeme)
    {
        printf("Veuillez choisir le modèle d'étude du mouvement\n");
        printf("tapez 1 pour Lorenz \ntapez 2 pour la trajectoire ballistique (quand on lance une balle de tennis dans une cloche à vide)\n");
        lire_entier_positif(numero_du_systeme);
    }
void demander_parametres_lorenz (float*theta , float*kho , float*beta)
{    
    printf ("Entrez les parametres theta, kho et beta :\n");
    printf("theta = ") ; lire_float(theta);
    printf("kho = ")   ; lire_float(kho)  ;
    printf("beta = ")  ; lire_float(beta) ;
}

void demander_parametres_generaux (float*x0 , float*y0 , float*z0 , float*Tmax , float*dt, int * nb_de_fleches)
{
    printf ("Entrez la position initiale du point x0, y0 et z0 en cartesien : \n" );
    printf("x0 = ") ; lire_float(x0);
    printf("y0 = ") ; lire_float(y0);
    printf("z0 = ") ; lire_float(z0);
    //coordonnes nouveaupoint( x , y , z);
    printf ("Entrez la durée de l'expérience Tmax et l'incrémentation de temps dt : \n");
    printf("durée de l'experience = ")   ; lire_float_positif(Tmax);
    printf("incrémentation de temps = ") ; lire_float_positif(dt);
    printf ("Entrez le nombre de flèches indiquant la vitesse en un point que vous souhaitez afficher, mettez-en trop à vos risques et périls!\n");
    printf("nombre de fleche = ") ; lire_entier_positif (nb_de_fleches);
}

void demander_parametres_trajectoire_ballistique ( float*g , float*vx , float*vy , float*vz)
{
   
    printf ("Entrez l'intensité du champ de pesanteur et la vitesse initiale vx , vy et vz :\n");
    printf("pesanteur = ") ;lire_float_positif(g);
    printf("vx = ") ; lire_float(vx);
    printf("vy = ") ; lire_float(vy);
    printf("vz = ") ; lire_float(vz);
}
